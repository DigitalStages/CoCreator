
import os
import cv2
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import StringProperty
from kivy.clock import Clock
from kivymd.app import MDApp
from kivymd.uix.floatlayout import MDFloatLayout
from kivymd.uix.menu.menu import MDDropdownMenu
from kivymd.uix.tab import MDTabsBase
from kivymd.uix.list import OneLineIconListItem
from videostream import VideoStream


class Tab(MDFloatLayout, MDTabsBase):
    '''Class implementing content for a tab.'''


class IconListItem(OneLineIconListItem):
    icon = StringProperty()


class SettingsApp(MDApp):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # List of names for tabs
        self.tab_labels = ["Cameras",
                           "Calibration",
                           "LED_Wand",
                           "Database",
                           "DL_Model"]
        # Initial setup for cameras preview using default image
        self.img_default = '../img/cam_ph.png'
        self.camera_images = {}
        self.camera_images['cam1'] = self.img_default
        self.camera_images['cam2'] = self.img_default
        self.camera_images['cam3'] = self.img_default
        # Initial list of camera devices
        self.camera_ids = []
        self.camera_res = ['1280x720', '960x540', '800x448', '640x480',
                           '640x360', '352x288', '320x240']
        self.chosen_cams = {'cam1': ['2', '640x480'],
                            'cam2': ['4', '640x480'],
                            'cam3': ['6', '640x480']
                            }
        self.check_available_cams()
        self.video_stream_cam1 = VideoStream()
        self.video_stream_cam2 = VideoStream()
        self.video_stream_cam3 = VideoStream()
        self.texture_stream_cam1 = None
        self.texture_stream_cam2 = None
        self.texture_stream_cam3 = None

    def build(self):
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = "Teal"
        return Builder.load_file('../kv/settings.kv')

    def on_tab_switch(
        self, instance_tabs, instance_tab, instance_tab_label, tab_text
    ):
        '''Called when switching tabs.
        :type instance_tabs: <kivymd.uix.tab.MDTabs object>;
        :param instance_tab: <__main__.Tab object>;
        :param instance_tab_label: <kivymd.uix.tab.MDTabsLabel object>;
        :param tab_text: text or name icon of tab;
        '''

    def load_settings(self):
        print('Load settings')

    def save_settings(self):
        print('Save settings')

    def check_available_cams(self):
        '''Check all video devices (Linux only)'''
        devs = os.listdir('/dev')
        self.devices = [int(dev[-1]) for dev in devs
                        if dev.startswith('video')]
        self.camera_ids = sorted(self.devices)

    def drpdown(self, cam, cat, instance):
        self.cam = str(cam)
        self.cat = str(cat)
        if self.cat.endswith('dev') is True:
            self.menu_items = [
                 {
                    "viewclass": "IconListItem",
                    "icon": "camera",
                    "text": f"/dev/video{i}",
                    "height": dp(56),
                    "on_release": lambda x=f"/dev/video{i}": self.set_item(
                                                        self.cam, self.cat, x),
                    } for i in self.camera_ids
            ]
        if self.cat.endswith('res') is True:
            self.menu_items = [
                 {
                    "viewclass": "IconListItem",
                    "icon": "camera",
                    "text": i,
                    "height": dp(56),
                    "on_release": lambda x=i: self.set_item(
                                                        self.cam, self.cat, x),
                    } for i in self.camera_res
            ]
        self.camera_menu = MDDropdownMenu(items=self.menu_items,
                                          position='center',
                                          width_mult=4,
                                          size_hint=(None, None))
        self.camera_menu.caller = instance
        self.camera_menu.open()

    def set_item(self, cam, cat, text_item):
        '''Replaces text with chosen item'''
        if cam == 'cam1' and cat == 'dev':
            self.root.ids.cam1_dev.set_item(text_item)
            self.chosen_cams['cam1'][0] = text_item
        if cam == 'cam1' and cat == 'res':
            self.root.ids.cam1_res.set_item(text_item)
            self.chosen_cams['cam1'][1] = text_item
        if cam == 'cam2' and cat == 'dev':
            self.root.ids.cam2_dev.set_item(text_item)
            self.chosen_cams['cam2'][0] = text_item
        if cam == 'cam2' and cat == 'res':
            self.root.ids.cam2_res.set_item(text_item)
            self.chosen_cams['cam2'][1] = text_item
        if cam == 'cam3' and cat == 'dev':
            self.root.ids.cam3_dev.set_item(text_item)
            self.chosen_cams['cam3'][0] = text_item
        if cam == 'cam3' and cat == 'res':
            self.root.ids.cam3_res.set_item(text_item)
            self.chosen_cams['cam3'][1] = text_item
        print(self.chosen_cams)
        self.camera_menu.dismiss()

    def stream_control(self, cam):
        self.cam = cam
        self.dev = self.chosen_cams[self.cam][0]
        self.resolution = self.chosen_cams[self.cam][1]
        if self.cam == 'cam1':
            self.video_stream_cam1.toggle_connect()
            if self.video_stream_cam1.cam_on is True:
                self.root.ids.cam1_btn.text = 'Disconnect'
                self.root.ids.cam1_btn_label.text = f'Connected to {self.cam}'
                self.video_stream_cam1.set_cam(self.cam)
                self.video_stream_cam1.set_resolution(self.resolution)
                self.video_stream_cam1.setup_stream()
                self.video_stream_cam1.start_stream()
            if self.video_stream_cam1.cam_on is False:
                self.root.ids.cam1_btn.text = 'Connect'
                self.root.ids.cam1_btn_label.text = 'No device connected'
                self.cam1_img.source = self.camera_images['cam1']

    #  def update_images(self, cam):
    #     pass
    #     # self.cam = cam
    #     # if self.cam == 'cam1':
    #     #     print('lol')
    #     #     self.camera_images['cam1'] =
    #
    # def update(self, cam):
    #     pass
    #     # self.update_images(cam)


if __name__ == '__main__':
    app = SettingsApp()
    app.run()
    # app.update()
