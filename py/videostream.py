
import cv2
import numpy as np
import threading
from kivy.graphics.texture import Texture


class VideoStream:
    '''Setup, start, stop and extract frames from video source'''
    def __init__(self):
        self.cam_on = False
        self.dev = '/dev/video0'
        self.res = '640x480'
        self.texture = None

    def toggle_connect(self):
        if self.cam_on is False:
            self.cam_on = True
        else:
            self.cam_on = False

    def set_cam(self, dev):
        self.dev = dev

    def set_resolution(self, resolution):
        self.resolution = resolution
        self.res_w, self.res_h = self.resolution.split('x')

    def setup_stream(self):
        # define a video capture object
        self.camera_stream = cv2.VideoCapture()
        self.camera_stream.open(2, apiPreference=cv2.CAP_V4L2)
        self.camera_stream.set(cv2.CAP_PROP_FOURCC,
                               cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
        self.camera_stream.set(cv2.CAP_PROP_FRAME_WIDTH, int(self.res_w))
        self.camera_stream.set(cv2.CAP_PROP_FRAME_HEIGHT, int(self.res_h))
        self.camera_stream.set(cv2.CAP_PROP_FPS, 30.0)
        self.buffer = self.camera_stream.get(cv2.CAP_PROP_BUFFERSIZE)
        self.camera_stream.set(cv2.CAP_PROP_BUFFERSIZE, self.buffer)
        self.start_frame_number = 20
        self.camera_stream.set(cv2.CAP_PROP_POS_FRAMES,
                               self.start_frame_number)

    def start_stream(self):
        print(f'Start camera {self.dev}')
        self.camera_thread = threading.Thread(
            target=self.get_texture()).start()
        return self.camera_thread

    def get_texture(self):
        while self.camera_stream.isOpened():
            has_frame, self.frame = self.camera_stream.read()
            if has_frame:
                # convert it to texture
                buf1 = cv2.flip(self.frame, 0)
                buf = buf1.tostring()
                self.texture = Texture.create(size=(
                    self.frame.shape[1],
                    self.frame.shape[0]), colorfmt='bgr')
                self.texture.blit_buffer(buf, colorfmt='bgr',
                                         bufferfmt='ubyte')
                # return self.texture
            if self.cam_on is False:
                # When everything done, release the video capture object
                self.camera_stream.release()
                break

    # def get_frame(self):
    #     self.dtype = self.camera_stream.get(cv2.CAP_PROP_FORMAT)
    #     print(self.cam_on)
    #     while self.camera_stream.isOpened():
    #         has_frame, self.frame = self.camera_stream.read()
    #         if has_frame:
    #             self.nframe = np.frombuffer(self.frame.texture.pixels,
    #                                         np.uint8)
    #             self.nframe = self.nframe.reshape(int(self.res_w),
    #                                               int(self.res_w), 4)
    #             # gray = cv2.cvtColor(newvalue, cv2.COLOR_RGBA2GRAY)
    #             # success, self.buffer = cv2.imencode('.jpg', frame)
    #             # self.buffer.flatten()
    #             # self.arr = np.ndarray(shape=[int(self.res_w),
    #             #                              int(self.res_h),
    #             #                              3], dtype=np.uint8)
    #             # self.frame_array = self.arr.tostring()
    #             # self.texture.blit_buffer(frame,
    #             #                          bufferfmt="ubyte",
    #             #                          colorfmt="rgb")
    #             return self.nframe
    #         if self.cam_on is False:
        #         break
        # # When everything done, release the video capture object
        # self.camera_stream.release()
