
# Using cv::estimateAffine3D API in Opencv
# Using the steps mentioned here: OpenCV 2.4 estimateAffine3D in Python


# def get_rigid(src, dst): # Assumes both or Nx3 matrices
#     src_mean = src.mean(0)
#     dst_mean = dst.mean(0)
#     # Compute covariance
#     H = reduce(lambda s, (a,b) : s + np.outer(a, b), zip(src - src_mean, dst - dst_mean), np.zeros((3,3)))
#     u, s, v = np.linalg.svd(H)
#     R = v.T.dot(u.T) # Rotation
#     T = - R.dot(src_mean) + dst_mean # Translation
#     return np.hstack((R, T[:, np.newaxis]))
