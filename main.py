
import os
from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.screenmanager import Screen
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.floatlayout import MDFloatLayout
from kivymd.uix.widget import MDWidget
from kivy.core.window import Window
from subprocess import Popen, PIPE
from py import threedview

Window.size = (1280, 720)


class ThreeDPort(MDFloatLayout):
	'''3D View with preview of polylines'''


class CoCreatorApp(MDApp):
	def __init__(self, **kwargs):
		super().__init__(**kwargs)

	def build(self):
		self.theme_cls.theme_style = "Dark"
		self.theme_cls.primary_palette = "BlueGray"
		return Builder.load_file('kv/main.kv')

	def open_settings(self):
		global settingsApp
		print(os.get_exec_path())
		settingsApp = Popen('python3 py/settings.py', shell=True)

	def close_settings(self):
		global settingsApp
		settingsApp.kill()


if __name__ == '__main__':
	app = CoCreatorApp()
	app.run()
